const loc_url = 'http://xn--b1agiiekhql3i0a.xn--p1ai';
// const loc_url = 'http://test_yavnorilske.o.atwinta.ru';
const AUTH = {
//   username: 'atwinta',
//   password: 'qweasdqwe'
}

checkGuidME = async function (guid) {
  const axios = require('axios');
  const instance = axios.create({
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    },
    auth: AUTH

  });
  const response = await instance.post(`${loc_url}/api/1/isnewguid`, { guid: guid })

  return response.data === 1 ? true : false;

}

parseME = async function (rubricMap) {
  const URL = rubricMap.url;
  const RUBRIC = rubricMap.rubric;

  const osmosis = require('osmosis');
  let articlesList = []
  await osmosis
    .get(`${URL}/${RUBRIC}`)
    .find('.list-item')
    .set(
      {
        'title': '.b-list__item-title',
        'link': 'a@href',
      })
    .data(function (data) {
      articlesList.push(data);
    })
    
  return articlesList.map((item) => {
    let tempAr = item.link.split('/');
    let num = tempAr.pop().split('.')[0];
    let date = tempAr.pop();
    return { ...item, guid: `${date}-${num}` };
  });
}


parseTourismME = async function (rubricMap) {
  const URL = rubricMap.url;
  const RUBRIC = rubricMap.rubric;
  const osmosis = require('osmosis');
  let articlesList = []
  await osmosis
    .get(`${URL}/${RUBRIC}`)
    .find('.cell-list__list')
    .set(
      {
        'title': 'a span',
        'link': 'a@href',
      })
    .data(function (data) {
      articlesList.push(data);
    })
  return articlesList.map((item) => {
    let tempAr = item.link.split('/');
    let num = tempAr.pop().split('.')[0];
    let date = tempAr.pop();
    return { ...item, guid: `${date}-${num}` };
  });
}


parseSportME = async function (rubricMap) {
  const URL = rubricMap.url;
  // const RUBRIC = rubricMap.rubric;
  const osmosis = require('osmosis');
  let articlesList = []
  await osmosis
    .get(`${URL}`)
    .find('.cell-list__list')
    .set(
      {
        'title': 'a span',
        'link': 'a@href',
      })
    .data(function (data) {
      articlesList.push(data);
    })
    
  return articlesList.map((item) => {
    let tempAr = item.link.split('/');
    let num = tempAr.pop().split('.')[0];
    let date = tempAr.pop();
    return { ...item, guid: `${date}-${num}` };
  });
}



parseArticleME = function (link, rubricMap) {
  const URL = rubricMap.url;
  const osmosis2 = require('osmosis');
  let article = '';
  prom = new Promise(function (resolve, reject) {
    osmosis2
      .get(`${URL}${link}`)
      .find('div.endless')
      .set({
        'title': 'h1.article__title',
        'date': 'div.endless__item@data-published',
        'img': 'div.photoview__open@data-photoview-src',
        'text': ['div.article__text']
      })
      .data(function (data) {
        let tempAr = link.split('/');
        let num = tempAr.pop().split('.')[0];
        let date = tempAr.pop();
        let text = data.text;
        if (text[0] === '') {
          text.shift();
        }
        article = { ...data, text: text, link: `${URL}${link}`, rubric: rubricMap.rubric, guid: `${date}-${num}` };
        resolve(article);
      })
      .error(() => {
        reject('error')
      });
  });

  prom.then(
    result => {
      article = result;
    },
    error => {
    }
  );
  
  return prom

}




createMaterialME = async function (newMaterial, rubricMap) {
  const axios = require('axios');
  const instance = axios.create({
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    },
    auth: AUTH
  });



  data = {
    img: newMaterial.img
  }

  let img_url = '';
  try {
    const config = {
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
      },
      auth: AUTH
    }
    const response = await axios.post(`${loc_url}/api/1/uploadlink`, data, config)

    img_url = await response.data;
  } catch (error) {
  }




  try {
    const response2 = await instance.post(`${loc_url}/api/1/materials`, template(img_url, rubricMap, newMaterial))
    console.log('загружена новость');

  } catch (error2) {
  }

}


function template(img_url, rubricMap, newMaterial) {
  const content = '<p>' + newMaterial.text.join('</p><p>') + '</p>';
  let ii = 0;
  let description = '';
  while (description.length + newMaterial.text[ii].length < 250) {
    description = description + newMaterial.text[ii];
    ii = ii + 1;
  }
  let preview_url = null;
  let content_img = '';
  if (img_url.length > 0) {
    content_img = img_url;
    preview_url = img_url
  }

  const blocks = [
    {
      decor: '',
      items: [
        {
          content: content_img,
          position: 0,
          type: 'file',
        },
        {
          content: content,
          position: 1
        }
      ],
      position: 0,
      type: 'img-text'
    }
  ]

  const templateMaterial = {
    age_rating_id: null,
    author: {
      name: rubricMap.author
    },
    blocks: blocks,
    comment: newMaterial.link,
    comment_denied: 0,
    comment_hide: 0,
    description: description,
    name: newMaterial.title,
    pinned: null,
    poster_dates: null,
    preview_text: '',
    preview_url: preview_url,
    publication_at: convertDateString(newMaterial.date, rubricMap.rubric),
    region_id: rubricMap.rubricInPortal.region_id,
    rubric_id: rubricMap.rubricInPortal.rubric_id,
    section_id: rubricMap.rubricInPortal.section_id,
    slug: null,
    stat_char: 0,
    stat_read: 0,
    stat_view: 0,
    status: 3,
    tags: [],
    title: newMaterial.title,
    user_id: 1,
    guid: newMaterial.guid
  }

  return templateMaterial;

}

function convertDateString(dateString, rubric) {
  const moment = require('moment');
  let riaDate;

  
  riaDate = new Date(dateString);
  riaDate.setHours(riaDate.getHours());
  
  return moment(riaDate).format('YYYY-MM-DD HH:mm:ss');

}








async function parseRubric(rubricMap) {
  let dat;


  if (rubricMap.rubric != 'tourism') {
    dat = await parseME(rubricMap);
  } else {
    dat = await parseTourismME(rubricMap);
    
  }
  
  dat = dat.map(item=>{
    let link = item.link;
    if (item.link.indexOf('https') != -1) {
      link = item.link.substr(14)
    }
    return {link:link,guid:item.guid} 
 
  });
  
  console.log(dat)
  
  

  let i = dat.length - 1;
  while (i >= 0) {
    let item = dat[i];
    
    let guidIsNew = await checkGuidME(item.guid);
    if (guidIsNew) {
      try {
        let newItem;
        // if (rubricMap.rubric != 'tourism') {
        newItem = await parseArticleME(item.link, rubricMap);
        // } else {
          // newItem = await parseArticleTourismME(item.link, rubricMap);
        // }
        if (newItem != '') {
          createMaterialME(newItem, rubricMap);
        }


      } catch (error) {
      }

    }
    
    i = i - 1;
  }
}

async function parseAll() {
  const author = 'ria.ru';
  const url = 'https://ria.ru';
  const rubrics = [
    {
      rubric: 'world',
      rubricInPortal:
      {
        section_id: 2,
        region_id: 2,
        rubric_id: 8,
      }
    },
    {
      rubric: 'culture',
      rubricInPortal:
      {
        section_id: 2,
        region_id: 2,
        rubric_id: 15,
      }
    },
    {
      rubric: 'politics',
      rubricInPortal:
      {
        section_id: 2,
        region_id: 2,
        rubric_id: 8,
      }
    },
    {
      rubric: 'society',
      rubricInPortal:
      {
        section_id: 2,
        region_id: 2,
        rubric_id: 9,
      }
    },
    {
      rubric: 'economy',
      rubricInPortal:
      {
        section_id: 2,
        region_id: 1,
        rubric_id: 10,
      }
    },
    {
      rubric: 'science',
      rubricInPortal:
      {
        section_id: 2,
        region_id: 1,
        rubric_id: 13,
      }
    },
    {
      rubric: 'religion',
      rubricInPortal:
      {
        section_id: 2,
        region_id: 2,
        rubric_id: 8,
      }
    },
    {
      rubric: 'tourism',
      rubricInPortal:
      {
        section_id: 2,
        region_id: 1,
        rubric_id: 16,
      }
    },


  ];

 

  let i = 0;
  while (i < rubrics.length) {
    let rubric = rubrics[i];
    await parseRubric({ ...rubric, author: author, url: url });
    i = i + 1;
  }
}

async function parseSportRubric() {
  const rubricMap =
  {
    author: 'rsport.ria.ru',
    url: 'https://rsport.ria.ru',
    rubric: 'sport',
    rubricInPortal:
    {
      section_id: 2,
      region_id: 2,
      rubric_id: 12,
    }
  }

  dat = await parseSportME(rubricMap);
  
  dat = dat.map(item=>{
    let link = item.link;
    if (item.link.indexOf('https') != -1) {
      link = item.link.substr(21)
    }
    return {link:link,guid:item.guid} 
 
  });
  
  console.log(dat)


  let i = dat.length - 1;
  while (i >= 0) {
    let item = dat[i];
    let guidIsNew = await checkGuidME(item.guid);
    if (guidIsNew) {
      if (is_news(item.link)) {
        let newItem = await parseArticleME(item.link, rubricMap);
        createMaterialME(newItem, rubricMap);
      }
    }
    i = i - 1;
  }


}

function is_news(link) {
  notNews = [
    'interview',
    'blog_'
  ];
  linkAr = link.split('/');
  if (linkAr[0] === '') {
    linkAr.shift();
  }

  let isNews = true;
  notNews.forEach(item => {
    if (linkAr[0].indexOf(item) >= 0) {
      isNews = false;
    }
  });


  return isNews;
}


async function parseAllAndSport() {
  await parseAll();
  
  await parseSportRubric();
  console.log('done');
}


// parseAllAndSport();



var CronJob = require('cron').CronJob;
var job = new CronJob('20 * * * *', function() {
parseAllAndSport();
  }, function () {
   },
   true, /* Start the job right now */
   'Asia/Krasnoyarsk' /* Time zone of this job. */
 );

 job.start();
